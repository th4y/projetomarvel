//
//  Stories.swift
//  Marvel
//
//  Created by c94288a on 29/11/21.
//

import Foundation
struct Stories: Codable {
    //    let available: Int
    //    let collectionURI: String
    let items: [StoriesItem]
    let returned: Int
}

// MARK: - StoriesItem
struct StoriesItem: Codable {
    let resourceURI: String
    let name: String
    //    let type: ItemType
}

//enum ItemType: String, Codable {
//    case cover = "cover"
//    case empty = ""
//    case interiorStory = "interiorStory"
//}

