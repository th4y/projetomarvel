//
//  Tumbnail.swift
//  Marvel
//
//  Created by c94288a on 29/11/21.
//

import Foundation

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String
    let thumbnailExtension: String
    
    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

//enum Extension: String, Codable {
//    case gif = "gif"
//    case jpg = "jpg"
//}
