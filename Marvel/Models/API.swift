//
//  API.swift
//  Marvel
//
//  Created by c94288a on 29/11/21.
//

import Foundation
import UIKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG


protocol RequestAPIProtocol{
    func url() -> String
    func chamadaAPI(completion: @escaping (Welcome) -> ())

    
}

class API: RequestAPIProtocol {
    var offSet: Int = 0
    func MD5(string: String) -> String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func url() -> String {
        // API
        let baseURL = "http://gateway.marvel.com"
        let path = "v1/public/characters"
        
        // Auth
        let publicKey = Bundle.main.object(forInfoDictionaryKey: "publicKey") as! String
        let privateKey = Bundle.main.object(forInfoDictionaryKey: "privateKey") as! String
        
        // Timestamp
        let ts = Int(Date().timeIntervalSince1970)
        
        // Hash
        let content = String(ts) + privateKey + publicKey
        let hash = MD5(string: content)
        
        // URL
        let url = baseURL + "/" + path + "?" + "ts=\(ts)" + "&apikey=\(publicKey)" + "&hash=\(hash)" + "&offset=\(offSet)"
       print(url)
        return(url)
    }
    
    
    func chamadaAPI(completion: @escaping (Welcome) -> ()){
        if let url = URL(string: url()) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let responseData = data else { return }
                // if let responseString = String(data: responseData, encoding: .utf8)
                
                do{
                    let heros = try JSONDecoder().decode(Welcome.self, from:  responseData)
                    self.offSet = self.offSet + 20
                    completion(heros)
                    //            print("objects heros: \(heros)")
                } catch let error {
                    print("error: \(error) ")
                }
            }
            .resume()
        }
    }
}
