//
//  Response.swift
//  Marvel
//
//  Created by c94288a on 29/11/21.
//

import Foundation
struct Welcome: Codable {
    let code: Int
    let status: String
    let data: DataClass
}
// MARK: - DataClass
struct DataClass: Codable {
    let offset, limit, total, count: Int
    let results: [Result]
}
// MARK: - Result
struct Result: Codable {
    let id: Int
    let name: String?
    let thumbnail: Thumbnail
    let stories: Stories
    // let urls: [URLElement]
    //
    //    enum CodingKeys: String, CodingKey {
    //        case id, name
    //        case resultDescription = "description"
    //        case modified, thumbnail, resourceURI,  stories
    
    
    //}
    
}
