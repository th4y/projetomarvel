//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by c94288a on 06/12/21.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {

    let apiMock = ApiMock()
    let databaseCapturer = DatabaseMock()
    let network = NetworkReachabilityMock(isReachable: true)
    let viewController = ViewControllerMock()
    
    func testFirstRequestAPISuccess() {

        let viewModel = HomeViewModel(api: apiMock, viewControllerProtocol: viewController, network: network, coreDataBaseController: databaseCapturer)

        viewModel.preencheArray() // Chamada do método a ser testado

        XCTAssertEqual(viewModel.arrayTotal.count, 20)
        XCTAssertTrue(databaseCapturer.saveCalled)

        XCTAssertEqual(viewModel.arrayHorizontal.count, 5)
        XCTAssertEqual(viewModel.arraVertical.count, 15)

        XCTAssertTrue(viewController.reloadCaroselCalled && viewController.reloadListCalled)

    }
    
    func testLoadContentNetworkIsReachable() {
        let viewModel = HomeViewModel(api: apiMock, viewControllerProtocol: viewController, network: network, coreDataBaseController: databaseCapturer)
        
        viewModel.setDataSource()

        XCTAssertTrue(apiMock.callAPICalled)
    }
    
    func getHerosOnCoreData(){
        let viewModel = HomeViewModel(api: apiMock, viewControllerProtocol: viewController, network: network, coreDataBaseController: databaseCapturer)
        
        viewModel.getHerosCoreDataLocal()
        XCTAssertEqual(viewModel.arrayHerosDB.count, 20)

        
        
        
    }
    
}
