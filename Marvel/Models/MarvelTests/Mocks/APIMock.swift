//
//  APIMock.swift
//  Marvel
//
//  Created by c94288a on 20/01/22.
//

import Foundation
//@testable import Marvel

class ApiMock: RequestAPIProtocol {
    var callAPICalled: Bool = false

    var isRequest: Bool = false
    
    func url() -> String {
        return "Ok"
    }
    
    func chamadaAPI(completion: @escaping (Welcome) -> ()) {
        callAPICalled = true
        
        guard let responseUrl = Bundle.main.url(forResource: "Mock", withExtension: "json") else {return}
        guard let jsonData = try? Data(contentsOf: responseUrl) else {return}
        
        do{
            let data = try JSONDecoder().decode(Welcome.self, from: jsonData)
            completion(data)
        }catch{
            print(error.localizedDescription)
        }
        
    }
}
    
    
