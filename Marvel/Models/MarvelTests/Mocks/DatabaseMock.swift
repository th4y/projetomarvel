//
//  DatabaseMock.swift
//  Marvel
//
//  Created by c94288a on 20/01/22.
//

import Foundation
class DatabaseMock: CoreDataProtocol{
    var context = DataBaseController.persistentContainer.newBackgroundContext()
    
    var saveCalled: Bool = false

    func saveHerosInCoreData() {
            saveCalled = true
    }

    
    func getHeroesOnCoreData() -> [HerosDB] {
        let hero = HerosDB(context: context)
        return Array(repeating: hero, count: 20)
    }
   
}
