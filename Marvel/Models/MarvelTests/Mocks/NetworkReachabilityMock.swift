//
//  NetworkReachabilityMock.swift
//  Marvel
//
//  Created by c94288a on 20/01/22.
//

import Foundation
class NetworkReachabilityMock: NetworkProtocol {
    var isReachable: Bool
    
    init(isReachable: Bool) {
        self.isReachable = isReachable
    }

    func startMonitoring() {
        isReachable = true
    }
    
    func stopMonitoring() {
        isReachable = true

    }
    

}
