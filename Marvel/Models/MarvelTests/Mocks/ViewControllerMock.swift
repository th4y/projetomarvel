//
//  ViewControllerMock.swift
//  Marvel
//
//  Created by c94288a on 20/01/22.
//

import Foundation
class ViewControllerMock: ViewControllerProtocol{
    var reloadCaroselCalled = false
    var reloadListCalled = false
    
    func reloadDataCarrossel() {
        reloadCaroselCalled = true

    }
    
    func reloadDataListaVertical() {
        reloadListCalled = true

    }
    
    
}

