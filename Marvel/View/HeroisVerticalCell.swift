//
//  HeroisVerticalCell.swift
//  Marvel
//
//  Created by c94288a on 20/11/21.
//

import UIKit

class HeroisVerticalCell: UICollectionViewCell {
    @IBOutlet weak var labelHeroi: UILabel!
    @IBOutlet weak var imageHeroi: UIImageView!
    
    func configCellVertical(with personagem: HeroisVertical){
        imageHeroi.image = UIImage(named: personagem.imagem ?? "")
        labelHeroi.text = personagem.nome
        
    }
    
    func cellVertical( infoHerois: Result){
        imageHeroi.image = UIImage(named: "2")
        labelHeroi.text = infoHerois.name
        
        labelHeroi.text = infoHerois.name
        let path = infoHerois.thumbnail.path
        let size = "standard_amazing"
        let extensionURL = infoHerois.thumbnail.thumbnailExtension
        let stringURL = "\(path)/\(size).\(extensionURL)"
        
        guard let url: URL = URL(string: stringURL) else {return}
        
        imageHeroi.kf.setImage(with: url, placeholder: UIImage (named: ""), options: [ .transition(.fade(2.0))], progressBlock: nil, completionHandler: nil)
        imageHeroi.layer.cornerRadius = 15.0
    }
}
