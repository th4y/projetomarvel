//
//  CarroselHorizontalCell.swift
//  Marvel
//
//  Created by c94288a on 20/11/21.
//

import UIKit
import Kingfisher

class CarroselHorizontalCell: UICollectionViewCell {
    
    @IBOutlet weak var imageHeroi: UIImageView!
    @IBOutlet weak var labelHeroi: UILabel!
    
    
    func cellHorizontal(infoHerois: Result ){
        labelHeroi.text = infoHerois.name
        let path = infoHerois.thumbnail.path
        let size = "standard_amazing"
        let extensionURL = infoHerois.thumbnail.thumbnailExtension
        let stringURL = "\(path)/\(size).\(extensionURL)"
        
        guard let url: URL = URL(string: stringURL) else {return}
        
        imageHeroi.kf.setImage(with: url, placeholder: UIImage (named: ""), options: [ .transition(.fade(2.0))], progressBlock: nil, completionHandler: nil)
        imageHeroi.layer.cornerRadius = 15.0
        
    }
}

