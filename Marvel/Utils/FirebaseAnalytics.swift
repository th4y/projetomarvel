//
//  FirebaseAnalytics.swift
//  Marvel
//
//  Created by c94288a on 01/02/22.
//

import Foundation
import FirebaseAnalytics

protocol FirebaseAnalytics{
    func eventSelectContent(key: String, value: String)
    func eventScreenView(screenName: String, screenClass: String)
    func eventSetUserIDAndSetUserProperty(userID: String, value: String, forName: String)
}

extension FirebaseAnalytics{
    
    func eventSelectContent(key: String, value: String){
        Analytics.logEvent(AnalyticsEventSelectContent,
                           parameters: [key: value])
    }
    
    func eventScreenView(screenName: String, screenClass: String){
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenName: screenName,
                                       AnalyticsParameterScreenClass: screenClass])
        
    }
    
    func eventSetUserIDAndSetUserProperty(userID: String, value: String, forName: String){
        Analytics.setUserID(userID)
        Analytics.setUserProperty(value, forName: forName)
    }
    
    
}
