//
//  ViewModel.swift
//  Marvel
//
//  Created by c94288a on 14/01/22.
//

import Foundation

protocol ViewModelProtocol {
    func preencheArray()
    func setDataSource()
    var heroFiltered: [Result]  {get set}
    var arrayTotal: [Result] {get set}
    var arrayHorizontal: [Result] {get set}
    var arraVertical: [Result] {get set}
    var isRequest: Bool { get }

}

class HomeViewModel: ViewModelProtocol {

    var heroFiltered: [Result] = []
    var arrayTotal: [Result] = []
    var arrayHorizontal: [Result] = []
    var arraVertical: [Result] = []
    var isRequest: Bool = false

    let context = DataBaseController.persistentContainer.viewContext
    var arrayHerosDB: [HerosDB] = []
    var offSet = 0
        
    var viewControllerProtocol: ViewControllerProtocol
    let api: RequestAPIProtocol
    let coreDataBaseController: CoreDataProtocol
    var network: NetworkProtocol
    
    init(api: RequestAPIProtocol, viewControllerProtocol: ViewControllerProtocol, network: NetworkProtocol, coreDataBaseController: CoreDataProtocol) {
        self.api = api
        self.viewControllerProtocol = viewControllerProtocol
        self.network = network
        self.coreDataBaseController = coreDataBaseController
    }
    
    
     func getHerosCoreDataLocal() {
        let heroesData: [HerosDB] = coreDataBaseController.getHeroesOnCoreData()
        var heroes: [Result] = []
        for heroData in heroesData {
            if heroData.nameHero != nil {
                var storiesItems: [StoriesItem] = []
                let heroDataStories = heroData.stories?.allObjects as? [DataStories]
                for heroDataStorie in heroDataStories!{
                    let items = StoriesItem(resourceURI: "", name: heroDataStorie.storieName!)
                    storiesItems.append(items)
                }
                
                let realStories = Stories(items: storiesItems, returned: storiesItems.count)
                let hero = Result(id: Int(heroData.id), name: heroData.nameHero ?? "", thumbnail: Thumbnail(path: heroData.pathImage ?? "", thumbnailExtension: heroData.extensaoImage ?? ""), stories: realStories )
                heroes.append(hero)
            }
            
        }
        if self.arrayHorizontal.isEmpty{
            self.arrayHorizontal = Array(heroes.prefix(5))
            
            if heroes.count >= 6 {
                self.arraVertical = Array(heroes.suffix(from: 6))
                self.heroFiltered = self.arraVertical
            }
            
        }else{
            self.arraVertical.append(contentsOf: self.arrayTotal)
            self.heroFiltered = self.arraVertical
        }
        self.viewControllerProtocol.reloadDataCarrossel()
        self.viewControllerProtocol.reloadDataListaVertical()
        
    }
    
    func setDataSource(){
        if network.isReachable{
            preencheArray()
        }else{
            getHerosCoreDataLocal()
        }
    }
    func preencheArray(){
        isRequest = true
        api.chamadaAPI() {(welcome) in
            self.arrayTotal = welcome.data.results
            for hero in self.arrayTotal {
                let heroData = HerosDB(context: self.context)
                heroData.nameHero = hero.name
                heroData.extensaoImage = hero.thumbnail.thumbnailExtension
                heroData.pathImage = hero.thumbnail.path
            }
         //  DispatchQueue.main.async{
                self.coreDataBaseController.saveHerosInCoreData()
          //  }
            if self.arrayHorizontal.isEmpty{
                self.arrayHorizontal = Array(self.arrayTotal.prefix(5))
                self.arraVertical = Array(self.arrayTotal.suffix(from: 5))
                self.heroFiltered = self.arraVertical
            }else{
                self.arraVertical.append(contentsOf: self.arrayTotal)
                self.heroFiltered = self.arraVertical
            }
            self.viewControllerProtocol.reloadDataCarrossel()
            self.viewControllerProtocol.reloadDataListaVertical()
            self.isRequest = false

        }
    }
}




