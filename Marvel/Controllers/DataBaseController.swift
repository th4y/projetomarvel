//
//  DataBaseController.swift
//  Marvel
//
//  Created by C94282A on 10/12/21.
//

import CoreData

class DataBaseController {
    // MARK: - Core Data stack
    
    public static var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "MarvelDB")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    public static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
