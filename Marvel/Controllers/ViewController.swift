//
//  ViewController.swift
//  Marvel
//
//  Created by c94288a on 20/11/21.
//

import UIKit
import CoreData

protocol ViewControllerProtocol: AnyObject {
    func reloadDataCarrossel()
    func reloadDataListaVertical()
}


class ViewController: UIViewController, UIScrollViewDelegate, ViewControllerProtocol, FirebaseAnalytics{
    var network = NetworkMonitor()
    let publicKey = Bundle.main.object(forInfoDictionaryKey:"publicKey") as! String

    
    lazy var viewModel : ViewModelProtocol = {
        let viewModel = HomeViewModel(api: API(), viewControllerProtocol: self, network: network, coreDataBaseController: CoreDataController())
        return viewModel
    }()
    
    
    
    ///MARKS: IBOutlet
    @IBOutlet weak var CarrosselHorizontal: UICollectionView!
    @IBOutlet weak var ListaVertical: UICollectionView!
    
    @IBOutlet weak var lupa: UIImageView!
    @IBOutlet weak var linhaDaBusca: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var featuredCharacters: UILabel!
    @IBOutlet weak var marvelCharactersList: UILabel!
    
    
    ///MARKS: Metodos
    override func viewDidLoad() {
        super.viewDidLoad()
        network.startMonitoring()
        configureCollections()
        viewModel.setDataSource()
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        headerNavigationBar()
        eventSetUserIDAndSetUserProperty(userID: publicKey, value: "Manga", forName: "favorite_fruit")
        eventScreenView(screenName: "main_screen", screenClass: "ViewController")
    }
    func headerNavigationBar(){
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.backgroundColor = .black
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 98, height: 39))
        imageView.contentMode = .scaleAspectFill
        let image = UIImage(named: "Logo")
        let layer = CALayer()
        layer.contents = image
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 0, c: 0, d: 1.01, tx: 0, ty: -0.01))
        layer.bounds = imageView.bounds
        layer.position = imageView.center
        imageView.layer.addSublayer(layer)
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    private func configureCollections(){
        CarrosselHorizontal.dataSource = self
        CarrosselHorizontal.delegate = self
        ListaVertical.dataSource = self
        ListaVertical.delegate = self
        self.searchBar.delegate = self
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = ListaVertical.contentOffset.y
        let contentHeight = ListaVertical.contentSize.height
        if offsetY > contentHeight - ListaVertical.frame.size.height && !viewModel.isRequest {
            viewModel.preencheArray()
        }
    }
}
extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CarrosselHorizontal{
            return viewModel.arrayHorizontal.count
        } else {
            return viewModel.arraVertical.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CarrosselHorizontal{
            let celula = collectionView.dequeueReusableCell(withReuseIdentifier: "horizoltalCell", for: indexPath) as! CarroselHorizontalCell
            celula.cellHorizontal(infoHerois: viewModel.arrayHorizontal[indexPath.row])
            return celula
        } else{
            let celula = collectionView.dequeueReusableCell(withReuseIdentifier: "verticalCell", for: indexPath) as! HeroisVerticalCell
            celula.cellVertical(infoHerois: viewModel.arraVertical[indexPath.row])
            return celula
        }
    }
}


extension ViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailViewController(viewModel.arrayHorizontal[indexPath.row])
        if collectionView == CarrosselHorizontal{
            let HeroSelectedCarousel: Result = viewModel.arrayHorizontal[indexPath.row]
            if let nameHero = HeroSelectedCarousel.name?.removingAccentsAndSpaces{
                eventSelectContent(key: "name_hero", value: nameHero)
            }
            detail.cellTocada = HeroSelectedCarousel
        } else{
            let HeroSelectedVertical: Result = viewModel.arraVertical[indexPath.row]
            if let nameHero = HeroSelectedVertical.name?.removingAccentsAndSpaces{
                eventSelectContent(key: "name_hero", value: nameHero)
            }
            detail.cellTocada = HeroSelectedVertical

        }
        self.navigationController?.pushViewController(detail, animated: true)
        
    }
}

extension ViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            let resultsFilter = viewModel.arraVertical.filter { $0.name?.hasPrefix(searchText) as! Bool }
            viewModel.arraVertical = resultsFilter
            ListaVertical.reloadData()
        } else {
            viewModel.preencheArray()
            //            arraVertical = self.heroFiltered
//            ListaVertical.reloadData()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        network.stopMonitoring()
    }
    
    func reloadDataCarrossel() {
        DispatchQueue.main.async {
            self.CarrosselHorizontal.reloadData()
        }
    }
    
    func reloadDataListaVertical() {
        DispatchQueue.main.async {
            self.ListaVertical.reloadData()
        }
    }
}


