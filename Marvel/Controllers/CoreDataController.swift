//
//  CoreDataController.swift
//  Marvel
//
//  Created by C94282A on 13/12/21.
//

import Foundation

protocol CoreDataProtocol{
    func saveHerosInCoreData()
    func getHeroesOnCoreData()->[HerosDB]
}

public class CoreDataController: CoreDataProtocol {
    
    let context = DataBaseController.persistentContainer.newBackgroundContext()
    
//        public func saveOffsetInCoreData(offset: Int32){
//            let newOffset = OffSet(context: context)
//            newOffset.offSet = offset
//            DataBaseController.saveContext()
//        }
//
//        public func getOffsetFromCoreData() -> Double{
//            var offset = 0
//            do {
//                let response : [OffSet] = try DataBaseController.persistentContainer.viewContext.fetch(OffSet.fetchRequest())
//                if response.count > 0{
//                    offset = Int(response[response.count-1].offSet)
//                }
//            } catch {
//                print("Sem informações no CoreData")
//            }
//            return Double(offset)
//        }
    
    public func saveHerosInCoreData(){
        context.automaticallyMergesChangesFromParent = true
        context.perform {
            do{
                try self.context.save()
                DataBaseController.saveContext()
            }catch let error {
                print(error)
            }
        }
    }
    
    public func getHeroesOnCoreData()->[HerosDB]{
        var data : [HerosDB] = []
        do {
            data = try DataBaseController.persistentContainer.viewContext.fetch(HerosDB.fetchRequest())
        } catch {
            print("erro")
        }
        return data
    }
}

