//
//  DetailViewController.swift
//  Marvel
//
//  Created by c94288a on 20/11/21.
//

import Foundation
import UIKit
class DetailViewController: UIViewController, FirebaseAnalytics{
    init(_ character: Result){
        self.cellTocada = character
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var cellTocada: Result?
    
    
    //    var nomeHeroi: String?
    //    var imagemHeroi: String?
    //    var descricaoHeroi: String?
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .black
        return scrollView
    }()
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
    }()
    private let imagemView: UIImageView = {
        let imagem = UIImageView()
        imagem.translatesAutoresizingMaskIntoConstraints = false
        imagem.image = UIImage(named: "2")
        let layer = CALayer()
        layer.contents = imagem
        layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 0, c: 0, d: 1.14, tx: 0, ty: 0))
        layer.bounds = imagem.bounds
        layer.position = imagem.center
        imagem.layer.addSublayer(layer)
        
        return imagem
    }()
    private let titleHeroiView: UIView = {
        let titleHeroiView = UIView()
        titleHeroiView.translatesAutoresizingMaskIntoConstraints = false
        titleHeroiView.backgroundColor = .black
        return titleHeroiView
    }()
    private let titleHeroiLabel: UILabel = {
        let titleHeroiLabel = UILabel()
        titleHeroiLabel.translatesAutoresizingMaskIntoConstraints = false
        titleHeroiLabel.textColor = .white
        titleHeroiLabel.font = UIFont.boldSystemFont(ofSize: 28.0)
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.61
        titleHeroiLabel.backgroundColor = .black
        titleHeroiLabel.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 181, height: 41))
        return titleHeroiLabel
    }()
    private let titleDescricaoView: UIView = {
        let titleDescricaoView = UIView()
        titleDescricaoView.translatesAutoresizingMaskIntoConstraints = false
        titleDescricaoView.backgroundColor = .black
        return titleDescricaoView
    }()
    private let titleDescricaoLabel: UILabel = {
        let titleDescricaoLabel = UILabel()
        titleDescricaoLabel.translatesAutoresizingMaskIntoConstraints = false
        titleDescricaoLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        titleDescricaoLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.14
        titleDescricaoLabel.backgroundColor = .black
        titleDescricaoLabel.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 84, height: 20))
        titleDescricaoLabel.attributedText = NSMutableAttributedString(string: "STORIES", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        return titleDescricaoLabel
        
        
    }()
    private let descricaoLabel: UILabel = {
        let descricaoLabel = UILabel()
        descricaoLabel.translatesAutoresizingMaskIntoConstraints = false
        descricaoLabel.numberOfLines = 0
        descricaoLabel.lineBreakMode = .byWordWrapping
        descricaoLabel.textColor = .white
        descricaoLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
        var paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.22
        descricaoLabel.backgroundColor = .black
        descricaoLabel.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 327, height: 1174))
        return descricaoLabel
    }()
    private let gradienteLabel: UILabel = {
        let gradienteLabel = UILabel()
        gradienteLabel.translatesAutoresizingMaskIntoConstraints = false
        return gradienteLabel
    }()
    private let descricaoView: UIView = {
        let descricaoView = UIView()
        descricaoView.translatesAutoresizingMaskIntoConstraints = false
        descricaoView.backgroundColor = .black
        return descricaoView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        setupView()
        setupData()
        headerNavigationBar()
    }
        
    override func viewDidAppear(_ animated: Bool) {
        eventScreenView(screenName: "detail_screen", screenClass: "DetailViewController.swift")
    }
        
 
        
        func setupData(){
            titleHeroiLabel.text = cellTocada?.name
            var descricao: String = ""
            for item in cellTocada!.stories.items{
                descricao += item.name + "\n"
            }
            descricaoLabel.text = descricao
            
            let path = cellTocada?.thumbnail.path
            let size = "standard_amazing"
            let extensionURL = cellTocada?.thumbnail.thumbnailExtension
            let stringURL = "\(path)/\(size).\(extensionURL)"
            
            guard let url: URL = URL(string: stringURL) else {return}
            
            imagemView.kf.setImage(with: url, placeholder: UIImage (named: ""), options: [ .transition(.fade(2.0))], progressBlock: nil, completionHandler: nil)
            //        descricaoLabel.text = descricao
        }
        func setupView() {
            view.addSubview(scrollView)
            NSLayoutConstraint.activate([
                scrollView.topAnchor.constraint(equalTo: view.topAnchor),
                scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
            scrollView.addSubview(stackView)
            NSLayoutConstraint.activate([
                stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
                stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
                stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
                stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
                stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
            ])
            stackView.addArrangedSubview(imagemView)
            NSLayoutConstraint.activate([
                imagemView.widthAnchor.constraint(equalToConstant: 375),
                imagemView.heightAnchor.constraint(equalToConstant: 375)
            ])
            //        imagemView.addSubview(gradienteLabel)
            //        NSLayoutConstraint.activate([
            //            gradienteLabel.widthAnchor.constraint(equalToConstant: 375),
            //            gradienteLabel.heightAnchor.constraint(equalToConstant: 375)
            //        ])
            
            stackView.addArrangedSubview(titleHeroiView)
            stackView.addArrangedSubview(titleDescricaoView)
            stackView.addArrangedSubview(descricaoView)
            
            titleHeroiView.addSubview(titleHeroiLabel)
            NSLayoutConstraint.activate([
                titleHeroiLabel.rightAnchor.constraint(equalTo: titleHeroiView.rightAnchor, constant: -24),
                titleHeroiLabel.leftAnchor.constraint(equalTo: titleHeroiView.leftAnchor, constant: 24),
                titleHeroiLabel.bottomAnchor.constraint(equalTo: titleHeroiView.bottomAnchor, constant: -24),
                titleHeroiLabel.topAnchor.constraint(equalTo: titleHeroiView.topAnchor, constant: 24)
            ])
            titleDescricaoView.addSubview(titleDescricaoLabel)
            NSLayoutConstraint.activate([
                titleDescricaoLabel.topAnchor.constraint(equalTo: titleDescricaoView.topAnchor, constant: 8),
                titleDescricaoLabel.leftAnchor.constraint(equalTo: titleDescricaoView.leftAnchor, constant: 24),
                titleDescricaoLabel.rightAnchor.constraint(equalTo: titleDescricaoView.rightAnchor, constant: -24),
                titleDescricaoLabel.bottomAnchor.constraint(equalTo: titleDescricaoView.bottomAnchor, constant: -8),
            ])
            descricaoView.addSubview(descricaoLabel)
            NSLayoutConstraint.activate([
                descricaoLabel.leadingAnchor.constraint(equalTo: descricaoView.leadingAnchor, constant: 24),
                descricaoLabel.topAnchor.constraint(equalTo: descricaoView.topAnchor, constant: 8),
                descricaoLabel.bottomAnchor.constraint(equalTo: descricaoView.bottomAnchor, constant: -8),
                descricaoLabel.trailingAnchor.constraint(equalTo: descricaoView.trailingAnchor, constant: -24)
                
            ])
        }
        func headerNavigationBar(){
            let nav = self.navigationController?.navigationBar
            nav?.barStyle = UIBarStyle.black
            nav?.backgroundColor = .black
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 98, height: 39))
            imageView.contentMode = .scaleAspectFill
            let image = UIImage(named: "Logo")
            let layer = CALayer()
            layer.contents = image
            layer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 0, c: 0, d: 1.01, tx: 0, ty: -0.01))
            layer.bounds = imageView.bounds
            layer.position = imageView.center
            imageView.layer.addSublayer(layer)
            imageView.image = image
            navigationItem.titleView = imageView
        }
        
    }

